/// \mainpage Documentation de drunk_player
/// Drunk_player est un système de lecture de vidéo qui a trop bu. il lit les vidéos contenues dnas un dossier par morceaux, aléatoirement et parfois en transformant l'image.
///
/// Drun_player utilise la bibliothèque de traitement d'image <a href="https://opencv.org/" >OpenCV</a> et est composé :
/// - d'une bibliothèque (drunk_player) contenant le code de base
/// - d'un programme graphique (drunk_player_gui) qui affiche le résultat à l'écran
/// - d'un programme console (drunk_player_cli) qui sort le résultat dans un fichier

