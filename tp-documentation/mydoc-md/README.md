# This is mydoc-md

## License
This project is under the MIT License (see [LICENSE.txt]())

## List
* item 1
* item 2

## Table
column1 | column2
------- | -------
foo | bar
toto | tata

## Code
```hs
main :: IO ()
main = putStrln "Hello"
```

## Quote
> I never said half the crap people said I did  
Albert Einstein

## Image
![image](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/1200px-Visual_Studio_Code_1.35_icon.svg.png)
