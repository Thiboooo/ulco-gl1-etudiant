#include "MyClass.hpp"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE( "init & getter", "[MyClass]" ) {
    // TODO créer un objet MyClass et testez le getter mydata
    MyClass myclass;
    REQUIRE ( myclass.mydata() == "");
}

TEST_CASE( "setter", "[MyClass]" ) {
    // TODO tester le setter mydata
    MyClass myclass;
    myclass.mydata() = "test";
    REQUIRE ( myclass.mydata() == "test");
}

TEST_CASE( "reset", "[MyClass]" ) {
    // TODO set puis reset puis tester
    MyClass myclass;
    myclass.mydata() = "test";
    myclass.reset();
    REQUIRE ( myclass.mydata() == "");
}

TEST_CASE( "fail", "[MyClass]" ) {
    // TODO tester que fail lance une exception (et tester le contenu de cette exception)
    MyClass myclass;
    try {
        myclass.fail();
        REQUIRE ( false );
    }
    catch (...) {
        REQUIRE (true);
    }
}

TEST_CASE( "sqrt2", "[MyClass]" ) {
    // TODO tester le résultat de sqrt2 à 0.001 près
    MyClass myclass;
    const double epsilon = 0.001;
    const double result = myclass.sqrt2();
    REQUIRE ( result < 1.414 + epsilon);
    REQUIRE ( result > 1.414 - epsilon);
}

TEST_CASE( "operator<<", "[MyClass]" ) {
    // TODO tester l'operateur << (après un set).
    // Indication : utiliser std::ostringstream.
}

