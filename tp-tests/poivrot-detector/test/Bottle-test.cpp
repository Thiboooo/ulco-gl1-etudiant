#include "../src/Bottle.hpp"
#include <iostream>
#include <vector>
#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE( "init Chimay" ) {
    const Bottle b {"Chimay", 0.75, 0.08};
    REQUIRE(b._name == "Chimay");
    REQUIRE(b._vol == 0.75);
    REQUIRE(b._deg == 0.08);
}

TEST_CASE( "init Orange Pur Jus" ) {
    const Bottle b {"Orange Pur Jus", 1, 0};
    REQUIRE(b._name == "Orange Pur Jus");
    REQUIRE(b._vol == 1);
    REQUIRE(b._deg == 0);
}

TEST_CASE( "alcoholVolume Chimay" ) {
    // TODO
    Bottle b { "Chimay" , .75, .08 };
    double resultat = alcoholVolume(b);
    REQUIRE(resultat == 0.06 );
}

TEST_CASE( "alcoholVolume Orange Pur Jus" ) {
    // TODO
    Bottle b { "Orange pur jus" , 1, 0 };
    double resultat = alcoholVolume(b);
    REQUIRE(resultat == 0 );
}

TEST_CASE( "alcoholVolume Chimay + Orange" ) {
    // TODO
    Bottle b1 { "Orange pur jus" , 1, 0 };
    Bottle b2 { "Chimay" , .75, .08 };
    std::vector<Bottle> bs;
    bs.push_back(b1);
    bs.push_back(b2);
    double resultat = alcoholVolume(bs);
    REQUIRE(resultat == 0.06);
}

TEST_CASE( "alcoholVolume Chimay + Chimay" ) {
    // TODO
    Bottle b1 { "Chimay" , .75, .08 };
    Bottle b2 { "Chimay" , .75, .08 };
    std::vector<Bottle> bs;
    bs.push_back(b1);
    bs.push_back(b2);
    double resultat = alcoholVolume(bs);
    REQUIRE(resultat == 0.12);
}

TEST_CASE( "isPoivrot Chimay + Orange" ) {
    // TODO
    Bottle b1 { "Orange pur jus" , 1, 0 };
    Bottle b2 { "Chimay" , .75, .08 };
    std::vector<Bottle> bs;
    bs.push_back(b1);
    bs.push_back(b2);
    REQUIRE(isPoivrot(bs));
}

TEST_CASE( "isPoivrot Chimay + Chimay" ) {
    // TODO
    Bottle b1 { "Chimay" , .75, .08 };
    Bottle b2 { "Chimay" , .75, .08 };
    std::vector<Bottle> bs;
    bs.push_back(b1);
    bs.push_back(b2);
    REQUIRE(isPoivrot(bs));
}

TEST_CASE( "readBottles Chimay" ) {
    // TODO
}

TEST_CASE( "readBottles Chimay + Orange" ) {
    // TODO
}

