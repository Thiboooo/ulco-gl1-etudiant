
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "Fibo.hpp"

TEST_CASE( " je teste fibo 0..4","[fibo]"){
    REQUIRE(fibo(0) == 0 );
    REQUIRE(fibo(1) == 1 );
    REQUIRE(fibo(2) == 1 );
    REQUIRE(fibo(3) == 2 );
    REQUIRE(fibo(4) == 3 );
    // REQUIRE( false ); //
}

TEST_CASE(" je teste fibo 5 ","[fibo]"){
    REQUIRE(fibo(5) == 5);
}