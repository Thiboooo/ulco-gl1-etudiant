#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <mymaths/mymaths.hpp>

TEST_CASE( " je teste mul2 2 4","[mul2]"){
    REQUIRE(mul2(2) == 4 );
    REQUIRE(mul2(4) == 8 );
    // REQUIRE( false ); //
}

TEST_CASE(" je teste muln 1,2","[muln]"){
    REQUIRE(muln(1,2) == 2);
}