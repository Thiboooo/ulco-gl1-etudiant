# Guess

## Description

Guess est un jeu qui a pour but de trouver un nombre aleatoire avec seulement 5 essaies .

guess est composé :
- d'une methode plusoumoins qui renvoie un oui ou non selon si le joueur a trouver le bon nombre et un affichage pour approcher le nombre
- d'une methode jeu qui va pouvoir lancer le jeu en utilisant la fonction plusoumoins


## Dépendances

- Aucune

## Compilation

```
Compilation grace a VS Code :
on selectionne l'environnement nix shell 
on prends non specifié en compilateur 
on appuie sur le rouage version

```

## Utilisation

```
on choisis la version que l'on veut lancer entre guess-test et guess-app 
( avec le bouton play sur vs code)
```