#include <guess/guess.hpp>
#include <iostream>

int plusoumoins(int n , int base){
    /// plusoumoins est une methode qui va renvoyer too high / too low / win / invalid en fonction du nombre que l'on test 
    /// win est un int qui joue un role de bool pour determiner la victoire
    /// n est le nombre choisis par le joueur 
    /// base est le nombre aleatoire choisis par la machine

    int win = 0;
    if (n > 100){
        std::cout << "invalid " << std::endl;
        return win;
    }
    else if ( n > base ){
        std::cout << "too high " << std::endl;
        return win;
    }else if  ( n < base ){
        std::cout << "too low " << std::endl;
        return win;
    }else if ( n == base ){
        std::cout << "win" << std::endl;
        win = 1;
        return win;
    }
}

void jeu(){
    /// jeu est une methode qui lancer une partie de jeu 
    /// compteur est un int qui compte le nombre de tour jouer 
    /// win est un int qui joue un role de bool pour determiner la victoire
    /// n est le nombre choisis par le joueur 
    /// alea est le nombre aleatoire choisis par la machine
    /// histo est un tableau de char qui va prendre toute les valeurs envoyer par le joueur et sauvegarder
    int compteur = 0;
    int win = 0;
    int n;
    int alea = rand() % 101;
    char histo[5] {0} ; 
    while ( compteur != 5 ){
        std::cout << "---------------" << std::endl;
        std::cout << "history:" << histo[compteur] << std::endl;
        std::cout << "number? " << std::endl;
        scanf("%d",&n);
        win = plusoumoins(n,alea);
        histo[compteur] = n;
        /*std::cout << "win ? " << win << std::endl;*/
        if (compteur == 4 ){
            std::cout << "target: " << alea << std::endl;
            break;
        }else if ( win == 1 ){
            break;
        }
        compteur ++;
        /*std::cout << "win ? " << win << std::endl;*/
    }


}
