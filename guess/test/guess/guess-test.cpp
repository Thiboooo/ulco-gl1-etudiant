#include <guess/guess.hpp>

#include <catch2/catch.hpp>


TEST_CASE( "test si plusoumoins renvoie  0 et aussi que ça cout invalid" ) {
    REQUIRE( plusoumoins(200,26) == 0 );
}

TEST_CASE( "test si plusoumoins renvoie  1 et aussi que ça cout invalid" ) {
    REQUIRE( plusoumoins(200,26) == 1 );
}

TEST_CASE( "test si plusoumoins renvoie  0 et aussi que ça cout too low" ) {
    REQUIRE( plusoumoins(9,26) == 0 );
}

TEST_CASE( "test si plusoumoins renvoie 1 et aussi que ça cout too low" ) {
    REQUIRE( plusoumoins(9,26) == 1 );
}

TEST_CASE( "test si plusoumoins renvoie 0 et aussi que ça cout win" ) {
    REQUIRE( plusoumoins(26,26) == 0 );
}

TEST_CASE( "test si plusoumoins renvoie 1 et aussi que ça cout win" ) {
    REQUIRE( plusoumoins(26,26) == 1 );
}

TEST_CASE( "test si plusoumoins renvoie 0 et aussi que ça cout too high" ) {
    REQUIRE( plusoumoins(80,26) == 0 );
}

TEST_CASE( "test si plusoumoins renvoie 1 et aussi que ça cout too high" ) {
    REQUIRE( plusoumoins(80,26) == 1 );
}

