.. Cesar documentation master file, created by
   sphinx-quickstart on Thu Dec  3 15:18:28 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cesar's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

CESAR

Le programme cesar est un programme qui est fait pour crypter et 
decrypter une fichier txt et renvoie le text decrypter dans un autre fichier txt

fonction:

- deplacerlettre , elle cripte une lettre en fonction d'un indice donner 
- cripter , elle cripte une ligne
- decripter , elle decripte une ligne
- frequence , renvoie un tableau de frequence de lettres 
- bruteforce , fonction en travaux mais elle a pour but de parcourir toute les possibilité de clef de cryptage et donné la clef la plus probable d'être la bonne 


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
