#include <cesar/cesar.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

int mul2(int n) {
    return n*2;
}

string deplacerlettre (int n , char a ){
    string res;
    if(n + int(a) > 122){ 
            a -= (122-n); 
            a = a % 26; 
            res += char(96 + a); 
        } 
        else
            res += char(n + a);
    return res;
}

string cripter ( string phrase ,int n ){
    int i;
    string res;
    for (i = 0; i < phrase.length(); i++){
        res += deplacerlettre(phrase[i],n);;
    }
    return res;
}

string decripter ( string phrase , int n ){
    return cripter ( phrase , -n);
}

vector<int> frequence( string phrase){
    vector<int> res = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    for ( int i = 0 ; i < phrase.length(); i++){
        for ( int j = 0 ; j < 26; j++){
            if ( phrase[i] == char(j)){
                res[i] = res[i] + 1;
            }
        }
    }
    return res;
}

int bruteforce(string phrase, int* freq){
    const vector<double> refFreqs {
        0.082, 0.015, 0.028, 0.043, 0.127, 0.022, 0.02, 0.061, 0.07,
        0.002, 0.008, 0.04, 0.024, 0.067, 0.075, 0.019, 0.001, 0.06,
        0.063, 0.091, 0.028, 0.01, 0.024, 0.002, 0.02, 0.001
    };
}
/* il faut trouver la plus petit clef de khi²*/