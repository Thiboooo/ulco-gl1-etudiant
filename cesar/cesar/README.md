# Guess

## Description

Cesar est un projet qui permet de cripter / decripter un fichier et resortir un fichier avec le code dechiffrer

cesar contient plusieurs methodes : 
    - deplacerlettre qui va cripter une lettre avec un indice donné
    - cripter qui cripte une ligne en utilisant deplacerlettre
    - decripter qui decripte une ligne en utilisant deplacerlettre
    - frequence , renvoie un tableau de frequence de lettres 
    - bruteforce , fonction en travaux mais elle a pour but de parcourir toute les possibilité de clef de cryptage et donné la clef la plus probable d'être la bonne 
## Compilation

```
Compilation grace a VS Code :
on selectionne l'environnement nix shell 
on prends non specifié en compilateur 
on appuie sur le rouage version

Pas besoin de rentrer de parametres , les parametres sont directement dans le programme !

```

## Utilisation

```
on choisis la version que l'on veut lancer entre guess-test et guess-app 
( avec le bouton play sur vs code)

Pas besoin de rentrer de parametres , les parametres sont directement dans le programme !
```