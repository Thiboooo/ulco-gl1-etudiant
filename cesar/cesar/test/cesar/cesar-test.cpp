#include <cesar/cesar.hpp>

#include <catch2/catch.hpp>

TEST_CASE( "deplacerlettre a 1 = b" ) {
    REQUIRE( deplacerlettre('a',1) == "b");
}

TEST_CASE( "deplacerlettre a 1 = a" ) {
    REQUIRE( deplacerlettre('a',1) == "a" );
}

TEST_CASE( "cripter aaaa 1 = aaaa " ) {
    REQUIRE( cripter("aaaa",1) == "aaaa" );
}

TEST_CASE( "cripter aaaa 1 = bbbb " ) {
    REQUIRE( cripter("aaaa",1) == "bbbb" );
}

TEST_CASE( "decripter bbbb 1 = bbbb " ) {
    REQUIRE( decripter("bbbb",1) == "bbbb" );
}

TEST_CASE( "decripter bbbb 1 = aaaa " ) {
    REQUIRE( decripter("bbbb",1) == "aaaa" );
}

/*TEST_CASE( "frequence de a dans aaaa" ) {
    REQUIRE( frequence("aaaa") == {4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0});
}

TEST_CASE( "frequence de a dans aaaa faux" ) {
    REQUIRE( frequence("aaaa") == {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0});
}*/
