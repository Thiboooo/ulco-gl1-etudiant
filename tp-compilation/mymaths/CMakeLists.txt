cmake_minimum_required( VERSION 3.0 )
project( mymaths )

include_directories( include )

add_library( mymaths src/mymaths/mymaths.cpp )

add_executable( app1 src/app1/main.cpp )
target_link_libraries( app1 mymaths )

add_executable( app2 src/app2/main.cpp )
target_link_libraries( app2 mymaths )